import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchItemWithInvalidProductCodeCSS {

	public static void main(String[] args) {
		
	System.setProperty("webdriver.chrome.driver","C://Users//USER//Documents//automationRavi//chromedriver.exe");
	
	WebDriver driver=new ChromeDriver();
	
	driver.get("https://tuclothing.sainsburys.co.uk/");
	//GOOD//
	
	driver.findElement(By.cssSelector("#search")).clear();
	driver.findElement(By.cssSelector("#search")).sendKeys("333333");
	driver.findElement(By.cssSelector(".button.searchButton")).click();
	
// remember the last line, it can be (button or , searchButton or,button.searchButton
	//Test Passed. as expected.
	
	
	
	


	}

}
