import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvalidSearchItem {

	public static void main(String[] args) {
		
			
			System.setProperty("webdriver.chrome.driver", "C://Users//USER//Documents//automationRavi//chromedriver.exe");
						
			WebDriver driver=new ChromeDriver();

			driver.get("https://tuclothing.sainsburys.co.uk/");

			driver.findElement(By.id("search")).clear();
			driver.findElement( By.id("search")).sendKeys("Biryani");
			driver.findElement(By.className("searchButton")).click();
	}

}
